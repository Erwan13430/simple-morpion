/***************************
 * Programme de jeu du morpion
 * composé par Falling_Knife
 * en août 2023
 */

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int checkForEmptyCells();
int checkForLines();
int playGame();
void printGrid();

char grille[9] = { '1', '2', '3', '4', '5', '6', '7', '8', '9' };

int main(int argc, char argv[]){
    srand(time(NULL));
    printf("Test\n");
    printGrid();
    int gagnant = playGame();
    if(gagnant == 1){
        printf("Félicitations, vous avez gagné la partie !\n");
    }else if(gagnant == 2){
        printf("Félicitations à l'ordinateur pour sa victoire !\n");
    }else{
        printf("Cette partie était un match nul. Bien joue !\n");
    }
    return 0;
}

void printGrid() {
    printf("\n");
    printf("   |   |\n");
    printf(" %c | %c | %c \n", grille[0], grille[1], grille[2]);
    printf("   |   |\n");
    printf("---+---+---\n");
    printf("   |   |\n");
    printf(" %c | %c | %c \n", grille[3], grille[4], grille[5]);
    printf("   |   |\n");
    printf("---+---+---\n");
    printf("   |   |\n");
    printf(" %c | %c | %c \n", grille[6], grille[7], grille[8]);
    printf("   |   |\n");
    printf("\n");
}

int playGame(){
    int resultat = 0; // Valeurs possibles de result: 0 -> nul, 1 -> joueur gagne, 2 -> Ordi gagne
    int case_joueur;
    int boucle_jeu = 1;
    int status_partie = 0;
    do{
        printf("************************\n");
        printf("*                      *\n");
        printf("*    TOUR DU JOUEUR    *\n");
        printf("*                      *\n");
        printf("************************\n");
        printf("\n");
        int case_joueur = 15;
        int boucle_case = 1;
        while(case_joueur > 9 || case_joueur < 1 || boucle_case == 1){
            printf("Veuillez choisir une case comprise entre 1 et 9 : ");
            scanf("%d", &case_joueur);
            if(case_joueur > 9 || case_joueur < 1){
                printf("Erreur : La case doit être comprise entre 1 et 9!\n");
            }else{
                if(grille[case_joueur - 1] == 'O' || grille[case_joueur - 1] == 'X'){
                    boucle_case = 1;
                    printf("Erreur : La case doit être vide (pas de 'X' ou de 'O')\n");
                }else{
                    boucle_case = 0;
                    grille[case_joueur - 1] = 'O';
                }
            }
        }
        printGrid();
        status_partie = checkForLines();
        if(status_partie != 0){
            boucle_jeu = 0;
            break;
        }else{
            if(checkForEmptyCells() == 0){
                boucle_jeu = 0;
                break;
            }
        }
        printf("************************\n");
        printf("*                      *\n");
        printf("* TOUR DE L'ORDINATEUR *\n");
        printf("*                      *\n");
        printf("************************\n");
        printf("\n");
        int case_ordi = rand() % 9;
        int boucle_case_ordi = 1; 
        while(boucle_case_ordi){
            if(grille[case_ordi] != 'O' && grille[case_ordi] != 'X'){
                boucle_case_ordi = 0;
                grille[case_ordi] = 'X';
            }else{
                case_ordi = rand() % 9;
                boucle_case_ordi = 1;
            }
        }
        printf("L'ordinateur choisi la case %d.\n", (case_ordi + 1));
        printGrid();
        status_partie = checkForLines();
        if(status_partie != 0){
            boucle_jeu = 0;
            break;
        }else{
            if(checkForEmptyCells() == 1){
                boucle_jeu = 1;
            }else{
                boucle_jeu = 0;
                break;
            }
        }
    }while(boucle_jeu);
    return status_partie;
}

int checkForLines(){
    if(grille[0] == grille[1] && grille[1] == grille[2]){
        if(grille[0] == 'O'){
            return 1;
        }else{
            return 2;
        }
    }else if(grille[3] == grille[4] && grille[4] == grille[5]){
        if(grille[3] == 'O'){
            return 1;
        }else{
            return 2;
        }
    }else if(grille[6] == grille[7] && grille[7] == grille[8]){
        if(grille[6] == 'O'){
            return 1;
        }else{
            return 2;
        }
    }else if(grille[0] == grille[3] && grille[3] == grille[6]){
        if(grille[0] == 'O'){
            return 1;
        }else{
            return 2;
        }
    }else if(grille[1] == grille[4] && grille[4] == grille[7]){
        if(grille[1] == 'O'){
            return 1;
        }else{
            return 2;
        }
    }else if(grille[2] == grille[5] && grille[5] == grille[8]){
        if(grille[2] == 'O'){
            return 1;
        }else{
            return 2;
        }
    }else if(grille[0] == grille[4] && grille[4] == grille[8]){
        if(grille[0] == 'O'){
            return 1;
        }else{
            return 2;
        }
    }else if(grille[2] == grille[4] && grille[4] == grille[6]){
        if(grille[2] == 'O'){
            return 1;
        }else{
            return 2;
        }
    }else{
        return 0;
    }   
}

int checkForEmptyCells(){
    if((grille[0] != 'O' && grille[0] != 'X') || (grille[1] != 'O' && grille[1] != 'X') || (grille[2] != 'O' && grille[2] != 'X') || (grille[3] != 'O' && grille[3] != 'X') || (grille[4] != 'O' && grille[4] != 'X') || (grille[5] != 'O' && grille[5] != 'X') || (grille[6] != 'O' && grille[6] != 'X') || (grille[7] != 'O' && grille[7] != 'X') || (grille[8] != 'O' && grille[8] != 'X')){
        return 1;
    }else{
        return 0;
    }
}
